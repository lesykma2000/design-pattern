#include<iostream>
#include<string>
#include<vector>
#include<algorithm>
class IObserver;
class ISubject;
class IObserver{
public:
    virtual void update(std::string data_)=0;
};
class ISubject{
public:
    virtual void registerObserver(IObserver *observer)=0;
    virtual void notifyObserver()=0;
    virtual void removeObserver(IObserver *observer)=0;
};

class Database:public ISubject{
public:
    std::vector<IObserver*>m_observerList;
    void registerObserver(IObserver *observer_){
        m_observerList.push_back(observer_);
    }

    void removeObserver(IObserver *observer){
        std::vector<IObserver*>::iterator it;
        it = std::find(m_observerList.begin(),m_observerList.end(),observer);
        m_observerList.erase(it);
    }

    void notifyObserver(){
        for(auto v:m_observerList){
            v->update(m_data);
        }
    }
    std::string m_data=" ";
    
    void editData(std::string newData){
        if(m_data.compare(newData)!=0){
            m_data = newData;
            notifyObserver();
        }
    }
};

class Student:public IObserver{
public:
    void update(std::string data_){
        std::cout<<"[student] data is: "<<data_<<std::endl;
    }
};

class Teacher:public IObserver{
public:
    void update(std::string data_){
        std::cout<<"[teacher] data is:"<<data_<<std::endl;
    }
};

int main()
{
    Database db;
    Student student;
    Teacher teacher;
    db.registerObserver(&student);    
    db.registerObserver(&teacher);
    db.editData("haha");        
    db.removeObserver(&teacher);
    db.editData("hihi");        

    return 0;
}